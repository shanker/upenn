#!/bin/bash
for i in `ls *pdf`; do gs -dNOPAUSE -sDEVICE=jpeg -sOutputFile="$i".jpg -dJPEGQ=100 -r500 -q "$i" -c quit; done
rm *pdf
