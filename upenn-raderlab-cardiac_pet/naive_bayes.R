#FUNCTIONS
#################################
#function to produce and save histograms, boxplots and correlation plots
#input parameter = data1, data2, data3, prefix
distributions <- function (lad,lcx,rca,prefix) {
  CL_lad <- (max(lad)-min(lad))/(1+3.332*(log (length(lad))))
  CL_lcx <- (max(lcx)-min(lcx))/(1+3.332*(log (length(lcx))))
  CL_rca <- (max(rca)-min(rca))/(1+3.332*(log (length(rca))))
  #Histograms
  pdf(file=paste(prefix,"-histogram.pdf",sep=""), width=12, height=4)
  par(mfrow=c(1,3))
  hist(lad, breaks=round(max(lad)/CL_lad), main=paste("Histogram: LAD",prefix,"\nClass interval=",round(CL_lad,digits=2),sep=" "), xlab="PET Signal intensity", ylab="frequency")
  hist(lcx, breaks=round(max(lcx)/CL_lcx), main=paste("Histogram: LCX",prefix,"\nClass interval=",round(CL_lcx,digits=2),sep=" "), xlab="PET Signal intensity", ylab="frequency")
  hist(rca, breaks=round(max(rca)/CL_rca), main=paste("Histogram: RCA",prefix,"\nClass interval=",round(CL_rca,digits=2),sep=" "), xlab="PET Signal intensity", ylab="frequency")
  dev.off()
  #Boxplots
  pdf(file=paste(prefix,"-boxplot.pdf",sep=""), width=12, height=4)
  par(mfrow=c(1,3))
  boxplot(lad, main=paste("Boxplot: LAD",prefix,sep=" "), xlab="LAD", ylab="PET Signal intensity")
  boxplot(lcx, main=paste("Boxplot: LCX",prefix,sep=" "), xlab="LCX", ylab="PET Signal intensity")
  boxplot(rca, main=paste("Boxplot: RCA",prefix,sep=" "), xlab="RCA", ylab="PET Signal intensity")
  dev.off()
  #Pearson's correlation coefficient
  lad_lcx_rca <- cbind(lad,lcx,rca)
  mycorr <- as.data.frame(capture.output(cor(lad_lcx_rca, use="complete.obs", method="pearson")))
  colnames(mycorr) <- ""
  pdf(file=paste(prefix,"-correlation.pdf",sep=""))
  pairs(lad_lcx_rca, main="Pearson's correlation coefficient")
  plot(c(0, 1), c(0, 1), main="Pearson's correlation coefficient matrix", bty = 'n', type = 'n', xaxt = 'n', yaxt = 'n')
  text(0,.5,paste(capture.output(mycorr), collapse='\n'), pos=4, family="mono")
  dev.off()
}
#################################
#function to produce and save comparisonwise density plots w.r.t. 2 classes
#input parameter = data1_class0, data1_class1, data2_class0, data2_class1, data3_class0, data3_class1, prefix
comparisonplot <- function (lad0,lad1,lcx0,lcx1,rca0,rca1,prefix) {
  pdf(file=paste(prefix,"-density.pdf",sep=""), width=12, height=4)
  par(mfrow=c(1,3))
  #LAD
  x_lad <- c(min(density(lad0)$y,density(lad1)$y), max(density(lad0)$x,density(lad1)$x)+0.02)
  y_lad <- c(0, max(density(lad0)$y,density(lad1)$y)+0.02)
  plot(density(lad0), main="", xlab="", ylab="", col="red", xlim=x_lad, ylim=y_lad)
  par(new=TRUE)
  plot(density(lad1), main=paste("LAD", toupper(prefix), sep=" "), xlab="PET Signal intensity", col="blue", xlim=x_lad, ylim=y_lad)
  text(x_lad[2], y_lad[2], pos=2, labels=paste("T-TEST, p-value=",format(t.test(lad0,lad1)$p.value, scientific=T, digits=3),sep=""))
  legend(x_lad[2], y_lad[2], xjust=1, yjust=1, c("Obstruction=0","Obstruction=1"), lty=c(1,1), col=c("red","blue"), bty="n")
  #LCX
  x_lcx <- c(min(density(lcx0)$y,density(lcx1)$y), max(density(lcx0)$x,density(lcx1)$x)+0.02)
  y_lcx <- c(0, max(density(lcx0)$y,density(lcx1)$y)+0.02)
  plot(density(lcx0), main="", xlab="", ylab="", col="red", xlim=x_lcx, ylim=y_lcx)
  par(new=TRUE)
  plot(density(lcx1), main=paste("LCX", toupper(prefix), sep=" "), xlab="PET Signal intensity", col="blue", xlim=x_lcx, ylim=y_lcx)
  text(x_lcx[2], y_lcx[2], pos=2, labels=paste("T-TEST, p-value=",format(t.test(lcx0,lcx1)$p.value, scientific=T, digits=3),sep=""))
  legend(x_lcx[2], y_lcx[2], xjust=1, yjust=1, c("Obstruction=0","Obstruction=1"), lty=c(1,1), col=c("red","blue"), bty="n")
  #RCA
  x_rca <- c(min(density(rca0)$y,density(rca1)$y), max(density(rca0)$x,density(rca1)$x)+0.02)
  y_rca <- c(0, max(density(rca0)$y,density(rca1)$y)+0.02)
  plot(density(rca0), main="", xlab="", ylab="", col="red", xlim=x_rca, ylim=y_rca)
  par(new=TRUE)
  plot(density(rca1), main=paste("RCA", toupper(prefix), sep=" "), xlab="PET Signal intensity", col="blue", xlim=x_rca, ylim=y_rca)
  text(x_rca[2], y_rca[2], pos=2, labels=paste("T-TEST, p-value=",format(t.test(rca0,rca1)$p.value, scientific=T, digits=3),sep=""))
  legend(x_rca[2], y_rca[2], xjust=1, yjust=1, c("Obstruction=0","Obstruction=1"), lty=c(1,1), col=c("red","blue"), bty="n")  
  dev.off()
}
#################################
#function produces discretized table
#input parameter = raw table, list of best-attributes and best-cutpoints as produced by function DcrtLst
#return value = discretized table
DcrtTable <- function (table, DcrtLst) {
  for(n in 1:ncol(table)) {                # iterate through table columns
    for(m in 1:length(DcrtLst)) {          # iterate through list of variables to be discretized
      if(colnames(table)[n] == DcrtLst[[m]][1]) {
        minimum <- min(as.numeric(table[,n]))
        maximum <- max(as.numeric(table[,n]))
        bounds <- list(minimum,maximum)
        for(l in 2:length(DcrtLst[[m]]))
          bounds <- append(bounds, as.numeric(DcrtLst[[m]][l]))
        b <- unlist(bounds)
        bounds <- b[order(b)]   #7.2  8.7 12.7 15.2
        for(k in 1:(length(bounds)-1)) {
          lower <- bounds[k]
          upper <- bounds[k+1]
          rng <- paste(lower, "..", upper, sep="")
          for(l in 1:length(table[,n]))
            if(!grepl("\\.\\.", table[l,n]))
              if(k==1 & as.numeric(table[l,n]) >= lower & as.numeric(table[l,n]) <= upper) {
                table[l,n] <- rng
              } else if(k>1 & as.numeric(table[l,n]) > lower & as.numeric(table[l,n]) <= upper) {
                  table[l,n] <- rng
              }
        }
      }
    }
  }
  return(table)
}
#################################
#function to produce and save barplots
#input parameter = data1, data2, data3, prefix
discretizedplot <- function(lad,lcx,rca,prefix) {
  #Barplots
  pdf(file=paste(prefix,"-barplot.pdf",sep=""), width=12, height=4)
  par(mfrow=c(1,3))
  barplot(table(lad), main=paste("Barplot: LAD",prefix,sep=" "), ylab="frequency", las=2)
  barplot(table(lcx), main=paste("Barplot: LCX",prefix,sep=" "), ylab="frequency", las=2)
  barplot(table(rca), main=paste("Barplot: RCA",prefix,sep=" "), ylab="frequency", las=2)
  dev.off()
}
#################################
#Function to compute likelihood estimates and normalized probability
#input parameter = conditional probability table (1 to n-1 rows for attributes CP; nth row for class CP)
#return value = Naive Bayes estimates table
likelihood <- function(cndPrb) {
  rowlabel <- vector(length = nrow(cndPrb))
  for(i in 1:nrow(cndPrb))
    rowlabel[i] <- paste("attr",i,sep="")
  likelihood_c <- vector(length=ncol(cndPrb)+1)
  probability_c <- vector(length=ncol(cndPrb)+1)
  for(i in 1:ncol(cndPrb))
    likelihood_c[i+1] <- prod(cndPrb[,i])
  for(j in 2:length(likelihood_c))
    probability_c[j] <- likelihood_c[j]/sum(likelihood_c[-1])
  likelihood_c[1] <- "likelihood"
  probability_c[1] <- "probability"
  subset1 <- cbind(rowlabel=as.character(rowlabel),cndPrb)
  subset2 <- rbind(as.character(likelihood_c), as.character(probability_c))
  colnames(subset2) <- colnames(subset1)
  return(rbind(subset1,subset2))
}
#################################
#MAIN
CardiacPET_territory <- read.delim2("CardiacPET_lad_lcx_rca.txt", na.strings=c("", "NA"), stringsAsFactors=FALSE, header=T)
CardiacPET_subset <- na.omit(CardiacPET_territory[,c("lad_stress","lcx_stress","rca_stress","global_stress",
                                                     "lad_rest","lcx_rest","rca_rest","global_rest",
                                                     "lad_reserve","lcx_reserve","rca_reserve","global_reserve","obstructive_lesion")])
#HISTOGRAMS
distributions(as.numeric(CardiacPET_subset$lad_rest),
              as.numeric(CardiacPET_subset$lcx_rest),
              as.numeric(CardiacPET_subset$rca_rest),
              "rest")
distributions(as.numeric(CardiacPET_subset$lad_stress),
              as.numeric(CardiacPET_subset$lcx_stress),
              as.numeric(CardiacPET_subset$rca_stress),
              "stress")
distributions(as.numeric(CardiacPET_subset$lad_reserve),
              as.numeric(CardiacPET_subset$lcx_reserve),
              as.numeric(CardiacPET_subset$rca_reserve),
              "reserve")
#COMPARISON PLOTS
comparisonplot(as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"lad_rest"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"lad_rest"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"lcx_rest"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"lcx_rest"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"rca_rest"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"rca_rest"]),
               "rest")
comparisonplot(as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"lad_stress"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"lad_stress"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"lcx_stress"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"lcx_stress"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"rca_stress"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"rca_stress"]),
               "stress")
comparisonplot(as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"lad_reserve"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"lad_reserve"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"lcx_reserve"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"lcx_reserve"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="0"),"rca_reserve"]),
               as.numeric(CardiacPET_subset[which(CardiacPET_subset[,"obstructive_lesion"]=="1"),"rca_reserve"]),
               "reserve")
#PRODUCE DISCRETIZED TABLE
cutpoints <- list(c("lad_rest", 0.12, 0.24, 0.36, 0.48, 0.60, 0.72, 0.84, 0.96, 1.08, 1.20, 1.32, 1.44, 1.56, 1.68, 1.80, 1.92, 2.04, 2.16, 2.28, 2.40),
                  c("lcx_rest", 0.12, 0.24, 0.36, 0.48, 0.60, 0.72, 0.84, 0.96, 1.08, 1.20, 1.32, 1.44, 1.56, 1.68, 1.80, 1.92, 2.04, 2.16, 2.28, 2.40),
                  c("rca_rest", 0.12, 0.24, 0.36, 0.48, 0.60, 0.72, 0.84, 0.96, 1.08, 1.20, 1.32, 1.44, 1.56, 1.68, 1.80, 1.92, 2.04, 2.16, 2.28, 2.40),
                  c("global_rest", 1.35, 1.6, 2.0),
                  c("lad_stress", 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00),
                  c("lcx_stress", 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00),
                  c("rca_stress", 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00),
                  c("global_stress", 1.35, 1.6, 2.0),
                  c("lad_reserve", 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00),
                  c("lcx_reserve", 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00),
                  c("rca_reserve", 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75, 4.00),
                  c("global_reserve", 1.35, 1.6, 2.0))
CardiacPET_discretized <- DcrtTable(CardiacPET_subset, cutpoints)
write.table(CardiacPET_discretized, "CardiacPET_discretized.txt", sep="\t", quote=FALSE, row.names=FALSE)
#BARPLOTS
discretizedplot(CardiacPET_discretized$lad_rest,
                CardiacPET_discretized$lcx_rest,
                CardiacPET_discretized$rca_rest,
                "rest")
discretizedplot(CardiacPET_discretized$lad_stress,
                CardiacPET_discretized$lcx_stress,
                CardiacPET_discretized$rca_stress,
                "stress")
discretizedplot(CardiacPET_discretized$lad_reserve,
                CardiacPET_discretized$lcx_reserve,
                CardiacPET_discretized$rca_reserve,
                "reserve")
#NAIVE BAYES
#Frequency table and conditional probabilities can be easily obtained from Weka's naive bayes classifier
#Conditional probabilities are used for prediction or diagnosis of new cases
#Territory probabilities for bins: lad=0.5..0.75, lcx=0.75..1, rca=1.75..2
territory_p <- data.frame(c0=c(0.022,0.054,0.155,0.465), c1=c(0.039,0.109,0.109,0.535))
naivebayes_terr <- likelihood(territory_p)
#################################
