; NAD world - knowledgebase

(defrule r1
   (sir2)
   =>
   (assert (life_span))
   (assert (caloric_restriction)))

(defrule r2
   (caloric_restriction)
   =>
   (assert (life_span)))

(defrule r3
   (glycolysis)
   ?pathway <- (glycolysis)
   (liver)
   (fasting)
   (sirt1)
   (PGC1a_Ac)
   =>
   (assert (PGC1a))
   (assert (Ac))
   (assert (gluconeogenesis))
   (retract ?pathway))

(defrule r4
   (liver)
   (fasting)
   (sirt1)
   (LXRa_Ac)
   =>
   (assert (LXRa))
   (assert (Ac))
   (assert (cholesterol_metabolism)))

(defrule r5
   (skeletal_muscle)
   (fasting)
   (sirt1)
   (PGC1a_Ac)
   =>
   (assert (PGC1a))
   (assert (Ac))
   (assert (fatty_acid_metabolism))
   )

(defrule r6
   ?factor <- (PTP1b)
   (skeletal_muscle)
   (sirt1)
   =>
   (assert (insulin_sensitivity))
   (retract ?factor)
   )

(defrule r7
   ?factor <- (PPARg)
   (white_adipose_tissue)
   (fasting)
   (sirt1)
   =>
   (assert (lipolysis))
   (assert (fatty_acid_mobilization))
	 (retract ?factor)
   )

(defrule r8
   (PPARg)
   =>
   (assert (adipogenesis))
   )

(defrule r9
   (white_adipose_tissue)
   (fasting)
   (sirt1)
   (PPARg)
   (FOXO1)
   =>
   (assert (adipokines))
   )

(defrule r10
   ?factor <- (Ucp2)
   (pancreatic_b)
   (fasting)
   (sirt1)
   =>
   (assert (glucose_stimulated_insulin_secretion))
	 (retract ?factor)
   )

(defrule r11
   ?pathway <- (glucose_stimulated_insulin_secretion)
   (Ucp2)
   =>
   (assert (ATP))
	 (retract ?pathway)
   )

(defrule r12
   (sirt1)
   (clock)
   (bmal1)
   =>
   (assert (period1))
   (assert (period2))
   (assert (period3))
   (assert (cytochrome1))
   (assert (cytochrome2))
   )

(defrule r13
   (invertebrate)
   (tryptophan)
   =>
   (assert (NAD))
   )

(defrule r14
   (invertebrate)
   (nicotinic_acid)
   =>
   (assert (NAD))
   )

(defrule r15
   (invertebrate)
   (nicotinamide)
   =>
   (assert (NAD))
   )

(defrule r16
   (invertebrate)
   (Pnc1)
   =>
   (assert (nicotinamidase))
   )

(defrule r17
   (invertebrate)
   (nicotinamidase)
   (nicotinamide)
   =>
   (assert (nicotinic_acid))
   (assert (amine))
   )

(defrule r18
   (invertebrate)
   (NAD)
   (sir2)
   =>
   (assert (nicotinamide))
   (assert (O_acetyl_ADP_ribose))
   )

(defrule r19
   (mammal)
   (nicotinamide)
   (Nampt)
   =>
   (assert (NMN))
   )

(defrule r20
   (mammal)
   (NMN)
   (Nmnat)
   =>
   (assert (NAD))
   )

(defrule r21
   (mammal)
   (NAD)
   (sirt1)
   =>
   (assert (nicotinamide))
   (assert (O_acetyl_ADP_ribose))
   )

(defrule r22
   (mammal)
   (pancreatic_b)
   (Nampt)
   =>
   (assert (glucose_stimulated_insulin_secretion))
   )

(defrule r23
   ?factor <- (Nampt)
   (FK866)
   =>
   (retract ?factor)
   )

