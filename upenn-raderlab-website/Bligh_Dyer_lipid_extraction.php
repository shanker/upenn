<!DOCTYPE html>
<html>
  <body>
    <h3 style='text-align:center'><a href="index.html">HOME</a></h3>
    <caption align=top style='text-align:left'><h1>BLIGH-DYER EXTRACTION FOR TISSUES OR AQUEOUS SAMPLES</h1></caption>
    <hr />
    <h3>PRINCIPLE</h3>
    <p>Bligh and Dyer (1959) propose a simple and rapid method for the extraction and purification of lipids from biological materials. The entire procedure takes approximately 30 minutes. The wet tissue or aqueous sample is homogenized with a mixture of chloroform and methanol in such proportions that a miscible system is formed with the water in the tissue. Dilution with chloroform and water separates the homogenate into two layers, the chloroform layer containing all the lipids and the methanolic layer containing all the non-lipids. A purified lipid extract is obtained merely by isolating the chloroform layer.</p>
    <hr />
    <h3>Please enter the tissue or sample volume for lipid extraction and then click NEXT button:</h3>
    <p>(pre-filled defaults are the most frequently used values)</p>
    <form action='Bligh_Dyer_lipid_extraction.php' method='post'>
      <input type=hidden name='tissue_vol_default' value='400'/><br/>
      Tissue or Aqueous sample, &mu;l <input type=var name='tissue_vol' placeholder='400' onFocus="value=''" value="<?php echo $_POST['tissue_vol'];?>"/><br/>
      <input type=reset value='reset'/> <input type=submit value='next' class='submit'/>
    </form>
    <hr />
  </body>
</html>
<?php
  include "background.html";
  $procedure = 'Bligh_Dyer_lipid_extraction';
//	if(!$_POST['tissue_vol']) { $_POST['tissue_vol'] = $_POST['tissue_vol_default']; }
  $tissue_vol = $_POST['tissue_vol'];
  if(!$_POST['tissue_vol']) { $tissue_vol = 400; }
  $water_vol = $tissue_vol * 1.25;
  $methanol_vol = $water_vol * 2;
  $chloroform_vol = $water_vol;
?>
<html>
  <body>
    <h3>CHEMICALS REQUIRED</h3>
    <pre>
			Tissue (mg)/Aqueous sample<?php echo "\t: ".$tissue_vol; ?> &mu;l
			Add methanol<?php echo "\t\t\t: ".$methanol_vol; ?> &mu;l
			Add chloroform<?php echo "\t\t\t: ".$chloroform_vol; ?> + <?php echo $chloroform_vol;?> &mu;l
			Add H<sub>2</sub>O<?php echo "\t\t\t\t: ".$water_vol; ?> &mu;l
    </pre>
    <h3>PROCEDURE</h3>
    <ol>
      <li>Final ratios should be in 1:1:0.9 (Chloroform:Methanol:Water/Tissue).<br><pre>	&#8594; Tissue (mg)/Aqueous sample<?php echo "\t".$tissue_vol; ?> &mu;l*		Vortex<br>	&#8594; Methanol<?php echo "\t\t\t".$methanol_vol; ?> &mu;l		Vortex<br>	&#8594; Chloroform<?php echo "\t\t\t".$chloroform_vol; ?> &mu;l		Vortex<br>	&#8594; Chloroform<?php echo "\t\t\t".$chloroform_vol; ?> &mu;l		Vortex<br>	&#8594; H<sub>2</sub>O<?php echo "\t\t\t\t".$water_vol; ?> &mu;l*		Vortex.<br><br>*These volumes can vary as long as they add up to <?php echo 0.9 * $methanol_vol; ?> &mu;l when combined.</pre>
      <li>Do not combine chloroform steps into one. Proper extraction is done with each one added separately.
      <li>After all additions and vortexing, centrifuge at 1000 rpm for 20 min at room temperature. Extraction is not as efficient at cold temperatures.
      <li>Remove bottom chloroform layer which contains lipids and dry under Nitrogen.
      <li>Resuspend lipids in desired solvent.
    </ol>
    <hr />
    <p><b>CREDIT</b>: Debra Cromley</p>
    <hr />
  </body>
</html>
