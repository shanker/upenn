<!DOCTYPE html>
<html>
  <body>
    <h3 style='text-align:center'><a href="index.html">HOME</a></h3>
    <caption align=top style='text-align:left'><h1>AGAROSE GEL ELECTROPHORESIS</h1></caption>
    <hr />
      <h3>PRINCIPLE</h3>
      <p>Gel electrophoresis is normally used in lab to separate DNA fragments. Agarose gel can separate fragments in the range from 500bp to 20,000bp and can resolve fragments differing upto 10bp. Polyacrylamide gels have much higher resolving power that can resolve fragments differing in size upto one nucleotide. Low percent gels are used for separating large DNA fragments while high percent gels are used to separate smaller sized fragment but high percent gel have low resolving power. DNA in gel is visualized with ethidium bromide (EtBr) dye solution. EtBr intercalates with DNA and when excited by UV light emits an orange-red fluorescence.</p>
      <hr />
      <h3>Please enter the desired volume and concentraton (%) of agarose gel required and then click NEXT button:</h3>
      <p>(pre-filled defaults are the most frequently used values)</p>
        <form action='gel_electrophoresis.php' method='post'>
          <input type=hidden name='gel_vol_default' value='40'/>
          <input type=hidden name='gel_conc_default' value='1'/>
          Volume (mL) <input type=var name='gel_vol' placeholder='40' onFocus="value=''" value="<?php echo $_POST['gel_vol'];?>"/><br/>
          Concentration (%) <input type=var name='gel_conc' placeholder='1' onFocus="value=''" value="<?php echo $_POST['gel_conc'];?>"/><br/>
          <input type=reset value='reset'/> <input type=submit value='next' class='submit'/>
        </form>
      <hr />
  </body>
</html>
<?php
  include "background.html";
  $procedure = 'gel_electrophoresis';
//	if(!$_POST['gel_vol']) { $_POST['gel_vol'] = $_POST['gel_vol_default']; }
//	if(!$_POST['gel_conc']) { $_POST['gel_conc'] = $_POST['gel_conc_default']; }
  $gel_vol = $_POST['gel_vol'];
  $gel_conc = $_POST['gel_conc'];
  if(!$_POST['gel_vol']) { $gel_vol = 40; }
  if(!$_POST['gel_conc']) { $gel_conc = 1; }
  $agarose_wt = ($gel_vol / 100) * $gel_conc;
  $etbr_vol = $gel_vol / 20;
?>
<html>
  <body>
    <h3>CHEMICALS REQUIRED</h3>
    <pre>
			TAE buffer (1X)			: <?php echo $gel_vol; ?> ml
			Agarose				: <?php echo $agarose_wt; ?> g
			Ethidium bromide (10mg/ml)	: <?php echo $etbr_vol; ?> &mu;l
    </pre>
    <h3>PROCEDURE</h3>
      <ol>
        <li>Preparing <?php echo $gel_vol; ?> ml of <?php echo $gel_conc; ?> % agarose gel solution: Add <?php echo $agarose_wt; ?> g of agarose in <?php echo $gel_vol; ?> ml of 1X TAE buffer <br><pre>	&#8594; boil with intermittent stirring <br>	&#8594; let it cool to about 50C <br>	&#8594; add <?php echo $etbr_vol; ?> &mu;l of ethidium bromide (10mg/ml).</pre>
        <li>Casting gel: Fix the gel casting try <br><pre>	&#8594; position the comb <br>	&#8594; pour the gel solution into the casting tray from one corner (Avoid bubbles).<br>	&#8594; let the gel cool until it solidifies.</pre>
        <li>When the gel has hardened, pour 1X TAE buffer on top of the gel to immerse it completely. Remove the comb carefully.
        <li>Now take about 8 &mu;l DNA sample and add 2 &mu;l electrophoresis dye. Mix thoroughly and load it in the well of gel. Load the size standard marker in another well.
        <li>Connect the leads of apparatus and run the electrophoresis at 100 volts for about 45 minutes or until the blue dye reaches about 1 cm from the bottom of the gel.
        <li>Turn off the power and take gel picture with the help of gel documentation system.
      </ol>
      <hr />
  </body>
</html>
