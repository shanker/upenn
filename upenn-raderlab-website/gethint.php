<!DOCTYPE html>
<?php
// Array with names<a href="index.html">HOME</a>
/*  $a[] = echo "<a href=www.google.com>Agarose gel electrophoresis</a>";*/
  $a[] = "Agarose gel electrophoresis";
  $a[] = "Plasmid mini-preparation";
  $a[] = "Plasmid restriction digestion and genetic mapping";
  $a[] = "Bligh-Dyer lipid extraction";
  $a[] = "ApoB and TG secretion study";
  $a[] = "TCA precipitation protocol";
  $a[] = "Serum Free Transfection of HEK293 Cells";
  $a[] = "Recombinant HDL (Proteoliposome) Preparation";
  $a[] = "LCAT Activity Assay";
  $a[] = "Primary Hepatocyte Isolation";
  $a[] = "Chemical inventory instructions";
// get the q parameter from URL
  $q = $_REQUEST["q"];
  $hint = "";
// lookup all hints from array if $q is different from ""
  if ($q !== "") {
    $q = strtolower($q);
    $len=strlen($q);
    foreach($a as $name) {
      if (stristr($q, substr($name, 0, $len))) {
        if ($hint === "") {
          $hint = $name;
        } else {
          $hint .= ", $name";
        }
      }
    }
  }
// Output "no suggestion" if no hint was found or output correct values
  echo $hint === "" ? "no suggestion" : $hint;
?> 
