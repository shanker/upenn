<!DOCTYPE html>
<?php
  include "background.html";
?>

<style type="text/css">
  img.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
</style>

<html>
  <body>
    <h3 style='text-align:center'><a href="index.html">HOME</a></h3>
    <caption align=top style='text-align:left'><h1>RESTRICTION ENZYME DIGESTION OF PLASMID DNA AND GENETIC MAPPING</h1></caption>
    <hr />
    <h3>BACKGROUND/PRINCIPLE</h3>
    <p>Werner Arber, Daniel Nathans and Hamilton Smith were awarded Nobel Prize for Medicine in 1978 for the discovery of restriction endonucleases. Restriction enzymes (RE) are endonucleases that attack double-stranded DNA at sites that contain specific sequences. Restriction enzymes recognize a stretch of DNA that contains a dyad axis of symmetry using only A, C, G and T, i.e., the sequence reads the same in the 5’ -> 3’ direction on each strand. Some cleave both strands exactly at the axis of symmetry,generating fragments of DNA that carry blunt ends; others cleave each strand at similar locations on opposite sides of the axis of symmetry, creating fragments of DNA that carry protruding single-stranded termini. For instance, the recognition sequence for E. coli RE, EcoRI is as follows:</p>
    <p style="text-align:center"><img src="EcoRI.png" alt="restriction digestion"></p>
    <p>The length of the sequence recognized by different restriction enzymes varies from 4 base pairs to 8 base pairs (or in some rare cases, as many as 13). Enzymes recognizing 4 base pairs will cut DNA more frequently than those with a longer recognition sequence. The optimal temperature for most restriction enzymes is at 370C, however, some require different temperatures.</p>
    <p>Typically, a restriction enzyme is a dimer and recognizes the two-fold axis of symmetry in the DNA at the target site. Because the bases are on the inside of the double helix, the restriction enzyme is thought to unwind the double helix. The α-helices of the restriction enzyme make specific contacts (via H-bonding) with the bases in the major groove. When all bases of the target site are recognized, the enzyme breaks two phosphodiester bonds, one on each chain.</p>
    <p>Restriction enzymes are produced by bacteria which uses it to defend itself from bacteriophage attack. When bacteriophage infects the bacteria, it inserts its DNA into the bacteria. Restriction enzymes present in the cytoplasm quickly recognizes specific sequences in the phage DNA and cuts it into many	pieces. Bacteria prevent their own DNA from being degraded by modifying their recognition sequences. Enzymes called methylases add methyl groups (-CH3) to adenine or cytosine bases within the recognition sequence, which is thus modified and protected from the endonuclease. The restriction enzyme and its corresponding methylase constitute the restriction- modification system of a bacterial species. Thus RE prevents phage infection and its replication inside bacteria by cutting specifically phage DNA and leaving bacterial DNA untouched.</p>
    <hr />
    <h3>CHEMICALS REQUIRED</h3>
    <ol>
      <li>Plasmid DNA</li>
      <li>Restriction enzymes – Hind III and Bam HI</li>
      <li>10X buffer</li>
      <li>ddH20</li>
      <li>1% Agarose gel</li>
      <li>1X TAE buffer</li>
      <li>Ethidium bromide</li>
      <li>Gel loading buffer</li>
    </ol>
    <hr />
    <h3>PROCEDURE</h3>
    <ol>
      <li>Label three 1.5ml tubes with a marker pen.</li>
      <li>Set up three reactions as shown (change tips between reagents) :</li><br>
      <table align='center' border=1 style='border-collapse:collapse; width:50%'>
        <tr>
          <th>Reagent, μl</th><th>Reaction 1</th><th>Reaction 2</th><th>Reaction 3</th>
        </tr>
        <tr>
          <td>10X buffer</td><td>2</td><td>2</td><td>2</td>
        </tr>
        <tr>
          <td>Plasmid DNA (.25μg/μl)</td><td>2</td><td>2</td><td>2</td>
        </tr>
        <tr>
          <td>ddH<sub>2</sub>O</td><td>15</td><td>15</td><td>14</td>
        </tr>
        <tr>
          <td>Hind III</td><td>1</td><td>-</td><td>1</td>
        </tr>
        <tr>
          <td>Bam HI</td><td>-</td><td>1</td><td>1</td>
        </tr>
        <tr align="left">
          <th>Total</th><th>20</th><th>20</th><th>20</th>
        </tr>
      </table><br>
      <li>Mix the contents by gentle vortex or tapping.</li>
      <li>Briefly spin to settle the contents at the bottom of tube and incubate them at 37C for 30-45 minutes.</li>
      <li>Cast 1% agarose gel as explained in experiment 1.</li>
      <li>Stop the digestion reactions by adding 4μl if gel loading buffer. Mix by pipetting and load entire volume (24μl) into the wells 2 through 4 in the agarose gel.</li>
      <li>Load size standard ladder in well 1.</li>
      <li>Connect the leads and run electrophoresis at 100 volts for 1 hour.</li>
      <li>After gel electrophoresis, photograph the gel with the help of gel documentation system.</li>
      <li>DO NOT DISCARD THE GEL. Wrap the gel in plastic wrapper and save in refrigerator until next lab to be used in Southern blotting experiment.</li>
      <li>Plot a standard curve for the 1kb ladder used based on the mobility of all bands and interpolate to estimate the sizes of the plasmid DNA fragments.</li>
      <li>Calculate the amount of DNA present in each band.</li>
      <li>Construct a restriction map of the plasmid DNA from the data.</li>
    </ol>
    <hr />
    <h3>RESULTS</h3>
    <img src="electrophoresis.png" alt="GEL image" style="width:304px;height:228px;" align="right">
    <h4>ELECTROPHORESIS GEL IMAGE</h4>
    <p>Plasmid digestion with single restriction enzyme	has produced single band while when it is digested with two different restriction enzymes, it produced two bands. This shows that the plasmid used for restriction digestion reaction is intact circular.</p>
    <h4 id='stdcrv'>STANDARD CURVE</h4>
    <p>The semi-log curve for the 1kb ladder on 1% agarose gel has shown linear relationship between log (weight) and migration rate of bands. When we interpolate migration distances of bands produced by plasmid DNA digestion, we obtain their molecular weights.</p>
    <img src="std_curve.png" alt="STANDARD-CURVE" style="width:304px;" align="right">
    <form action="standard_curve.php" method="post" enctype="multipart/form-data">
      Enter 1kb ladder fragment sizes (bp): <textarea name='fragment' cols='75' rows='2' value="<?php echo $_POST['fragment'] ?>" required>10000,8000,6000,5000,4000,3000,2500,2000,1500,1000,750,500,250</textarea><br>
      Enter distace travelled by 1kb ladder fragment sizes (cms): <textarea name='distance' cols='50' rows='2' value="<?php echo $_POST['distance'] ?>" required>0,0.35,0.65,1.05,1.4,2.1,2.5,3,3.7,4.65,5.3,6,7</textarea><br>
      Enter distace travelled by sample DNA fragment (cms): <input type=var name='band' size='5' value='1.02'/><br>
      <input type=submit value='Submit' class='submit'>
    </form>
    <h4>DNA CONTENT ESTIMATION</h4>
    <p>In principle we can estimate the amount of DNA present in band using λ/Hind III ladder given we know the total amount of ladder loaded in the gel. The circular λ DNA cut with Hind III produces fragments which sums to total length of λ DNA. Based on the individual fragment	length of the λ-ladder (which is well known), amount of DNA in each one of fragment band can be estimated. Finally, by comparing the band intensity of the sample band with bestmatch in ladder, the amount of DNA in sample can be predicted.</p>
    <p>If total of 0.4μg DNA is loaded, that means 48502bp band would represent 0.4μg DNA or 1bp represent 8.25 x 10<sup>-6</sup> μg DNA. In the same context, each fragment of λ/HIndIII digest would represent following amount of DNA:</p>
    <table align="center" border=0 style="width:25%">
      <tr><td>23130 x 8.25 x 10<sup>-6</sup> = 0.19 μg</td></tr>
      <tr><td>9416 x 8.25 x 10<sup>-6</sup> = 0.08 μg</td></tr>
      <tr><td>6557 x 8.25 x 10<sup>-6</sup> = 0.05 μg</td></tr>
      <tr><td>4361 x 8.25 x 10<sup>-6</sup> = 0.04 μg</td></tr>
      <tr><td>2322 x 8.25 x 10<sup>-6</sup> = 0.02 μg</td></tr>
      <tr><td>2027 x 8.25 x 10<sup>-6</sup> = 0.017 μg</td></tr>
      <tr><td>564 x 8.25 x 10<sup>-6</sup> = 0.005 μg</td></tr>
      <tr><td>125 x 8.25 x 10<sup>-6</sup> = 0.001 μg</td></tr>
    </table>
    <p>But this principle cannot be used when other DNA ladder such as 1kb ladder is used. This is because the source of fragments in these ladders is not from single molecule and thus do not sum to any constant value. In the present experiment, we have used 1 kb ladder and thus we cannot estimate the amount of plasmid DNA in respective bands in the gel.</p>
    <h4>RESTRICTION MAP</h4>
    <p>By using the standard curve of 1kb ladder, we estimated the length of different fragments produced by restriction enzyme digestions:</p>
    <table align="center" border=1 style="border-collapse:collapse; width:40%">
      <tr>
        <th>LANE #</th><th>ENZYME</th><th>FRAGMENT LENGTH</th>
      </tr>
      <tr>
        <td>2</td><td>Hind III</td><td>6309.6 bp</td>
      </tr>
      <tr>
        <td>3</td><td>Bam HI</td><td>6309.6 bp</td>
      </tr>
      <tr>
        <td>4</td><td>Hind III + Bam HI</td><td>5370.3 bp + 912.0 bp</td>
      </tr>
    </table>
    <p>The fragment lengths in lane 4 should sum to those in lane 2 and lane 3 (the difference is only due to the experimental error).</p>
    <p align="center">5370.3 + 912.0 = 6282.3 ≈ 6309.6</p>
    <p>Thus, based on above data the restriction map of plasmid DNA is as follows:</p>
    <p style="text-align:center"><img src="restriction_map.png" alt="restriction map"></p>
    <hr />
    <h3>DISCUSSION</h3>
    <p>Normally, 2 units of restriction enzyme is enough for digestion of 1μg of DNA but due to the pipettelimitations,	5 units of enzyme/1μg DNA digestion is usually used in laboratory. Since enzyme is stored in glycerol, it is thickly viscous and large amount sticks to the micropipette tip while transfer. Restriction enzymes are expensive and their wastage can be avoided by injecting volume from the top layer only without inserting pipette tip deep into the bottom of the tube. Another important concern is the amount of glycerol used in the experiment. Glycerol is not good for enzyme activity and if it is present above 5% in the experiment volume, it inhibits digestion and also causes star activity i.e., reduces restriction enzyme specificity. The optimum duration of digestion reaction is between 40 minutes to 3 hours depending on the restriction enzyme used. Digestion should not been prolonged overnight which causes too many cuts resulting from recognition of non-specific sites.</p>
    <hr /><br>
  </body>
</html>
