<html><body>
<?php
include "background.html";
$procedure = $_POST['procedure'];
switch ($procedure) {
	case 'gel_electrophoresis':
		include 'gel_electrophoresis.php';
		break;
	case 'plasmid_mini_prep':
		include 'plasmid_mini_prep.html';
		break;
	case 'plasmid_restriction_digestion':
		include 'plasmid_restriction_digestion.php';
		break;
	case 'Bligh_Dyer_lipid_extraction':
		include 'Bligh_Dyer_lipid_extraction.php';
		break;
	case 'tca_precipitation':
		include 'tca_precipitation.html';
		break;
	case 'apob_tg_secretion':
		include 'apob_tg_secretion.html';
		break;
	case 'serum_free_transfection_hek293':
		include 'serum_free_transfection_hek293.html';
		break;
	case 'recombinant_hdl_proteoliposome':
		include 'recombinant_hdl_proteoliposome.html';
		break;
	case 'lcat_activity':
		include 'lcat_activity.html';
		break;
	case 'primary_hepatocyte_isolation':
		include 'primary_hepatocyte_isolation.html';
		break;
	case 'cispro_live_chemical_inventory':
		include 'cispro_live_chemical_inventory.html';
		break;
	default:
		echo 'ERROR...';
		break;
}
?>
</body></html>
