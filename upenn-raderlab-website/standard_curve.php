<!DOCTYPE html>
<html>
  <h3 style='text-align:center'><a href="index.html">HOME</a></h3>
</html>
<?php
  include "background.html";
/*
echo("<pre>");
echo ini_get("memory_limit")."\n";
ini_set("memory_limit","2000M");
echo ini_get("memory_limit")."\n";
ini_set('memory_limit', '256M');
echo ini_get("max_execution_time")."\n";
ini_set("max_execution_time","120");
echo ini_get("max_execution_time")."\n";
$fragment="10000,8000,6000,5000,4000,3000,2500,2000,1500,1000,750,500,250";
$distance="0,0.35,0.65,1.05,1.4,2.1,2.5,3,3.7,4.65,5.3,6,7";
$bandDist="2";
*/
  $fragment=$_POST["fragment"];
  $distance=$_POST["distance"];
  $bandDist=$_POST["band"];
  $PreProcFragment=trim(ereg_replace("[[:space:]]{1,}",",", ereg_replace(",", " ", $fragment)));
  $PreProcDistance=trim(ereg_replace("[[:space:]]{1,}",",", ereg_replace(",", " ",$distance)));
  $y_var=explode(',',$PreProcFragment);
  $x_var=explode(',',$PreProcDistance);
  if (sizeof($y_var) != sizeof($x_var)) {
    $err_msg='X and Y variables are of unequal sizes';
  } else {
    $fragSize="c(".$PreProcFragment.")";
    $fragDist="c(".$PreProcDistance.")";
    require_once("connect.php");
    $connection = ssh2_connect($server, 22);
    ssh2_auth_password($connection, $username, $password);
    if (!$connection) {die('Connection failed')."\n";} else {
      $stream = ssh2_exec($connection, "bsub \"sh stdCur.sh '$fragDist' '$fragSize' '$bandDist'\"");
      stream_set_blocking($stream, true);
      fclose($stream);
      shell_exec (`sleep 8`);
      $remote_file_name = "std_curve.pdf";
      $local_file_name = "std_curve.pdf";
      $transfer = ssh2_connect($tfrserver, 22);
      ssh2_auth_password($transfer, $username, $password);
      if (!$transfer) {die('Transfer connection failed')."\n";} else {
        $stream2 = ssh2_scp_recv($transfer, "$remote_file_name", $local_file_name);
        stream_set_blocking($stream2, true);
        fclose($stream2);
      }
    }
//echo("<pre>");
//echo file_get_contents("lm_std_curve.txt");
//echo("</pre>");
  }
?>
<html>
  <style>
    th { text-align: left; }
    td { vertical-align: top; }
  </style>
  <table align="center" border=0 style="width:80%">
    <tr>
      <th style="width:510px"></th>
      <th style="width:200px"><h3>1kb-LADDER</h3><p>Fragment sizes (bp)</p></th>
      <th style="width:200px"><h3><br></h3><p>Distance travelled (cm)</p></th>
    </tr>
    <tr><td> <?php if (sizeof($y_var)!=sizeof($x_var)) { echo ("<html><h3>$err_msg</h3></html>"); } else { $nocache=rand(); echo("<embed src='std_curve.pdf?$nocache' width='500' height='500' type='application/pdf' class='center'/>"); } ?> </td><td> <?php echo ('<pre>'); print_r($y_var); echo ('</pre>'); ?> </td><td> <?php echo ('<pre>'); print_r($x_var); echo ('</pre>'); ?> </td></tr>
  </table>
  <h3 style='text-align:left'><a href="plasmid_restriction_digestion.php#stdcrv">BACK</a></h3>
</html>
