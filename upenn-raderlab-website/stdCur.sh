#!/bin/bash
#BSUB -J stdCur.sh         # LSF job name
#BSUB -o stdCur.%J.out     # Name of the job output file
#BSUB -e stdCur.%J.error   # Name of the job error file

rm *out *error std_curve*pdf
myvar1=$1
myvar2=$2
myvar3=$3
cmd="bsub R CMD BATCH --no-save --no-restore '--args "$myvar1 $myvar2 $myvar3"' stdCur.R"
echo `eval $cmd`

